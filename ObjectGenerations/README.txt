ObjectGenerations project shows an example of using maven code generators and schemas.

Prerequisites:
 -minimum JDK 1.7 (you can probably get by with jdk 6).
 -minimum Maven 3.0.5 (I used 3.2.4).

To run execute:

    mvn clean install

Check the targtet output for the generated classes.

Eclipse Luna project files are included.

Regards

 Bil Mowry
 Ivy Street Technology LLC
 November 2014
