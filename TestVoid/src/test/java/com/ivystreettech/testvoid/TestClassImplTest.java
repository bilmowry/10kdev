package com.ivystreettech.testvoid;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class TestClassImplTest {
	
	private TestClass testClass = new TestClassImpl();

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testMethodNoError() {
		
		Boolean testState = true;
		
		try {	
			testClass.methodNoError();	
		} catch (Exception e) {
			testState = false;
		}
		
		assertTrue(testState);
	}
	
	@Test
	public void testMethodError() {
		
		Boolean testState = true;
		
		try {	
			testClass.methodError();	
		} catch (Exception e) {
			testState = false;
		}
		
		assertFalse(testState);
	}
	
	@Test(expected = Exception.class)
	public void testMethodErrorExpected() {
		
			testClass.methodError();	

	}
	
	

}
