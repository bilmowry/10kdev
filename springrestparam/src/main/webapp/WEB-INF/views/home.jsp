<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Spring MVC IP Address Parameter Test</title>
    </head>
    <body>
        <p>This example shows how Spring MVC truncates an IP address passed in as a path variable, and three ways to correct this behavior.</p>
        <ul>
            <li><a href="/param/parsed/10.6.92.123" target="_blank" >FAIL path variable truncates ip address</a></li>
            <li><a href="/param/regex/10.6.92.123" target="_blank" >PASS regex path variable returns ip address</a></li>
            <li><a href="/param/servlet/10.6.92.123" target="_blank" >PASS HttpServletResponse returns ip address</a></li>
            <li><a href="/param/request?ipAddress=10.6.92.123" target="_blank" >PASS request param returns ip address</a></li>
        </ul>
    </body>
</html>
