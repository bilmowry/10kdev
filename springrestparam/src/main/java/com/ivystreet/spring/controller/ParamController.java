package com.ivystreet.spring.controller;

import com.ivystreet.spring.domain.IpResponse;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;


@Controller
@RequestMapping(value = "/param", produces = MediaType.APPLICATION_JSON_VALUE)
public class ParamController {

    /**
     * FAIL: Using @PathVariable teh ip address is truncated by Spring.
     *
     * URL:
     * http://localhost:8080/parsed/10.32.14.15
     *
     * Expected:
     * {"method":"parsed","ipAddress":"10.32.14"}
     */
    @RequestMapping(method = RequestMethod.GET, value = "/parsed/{ipAddress}")
    public @ResponseBody IpResponse parsedIp(@PathVariable String ipAddress) {
        IpResponse ip = new IpResponse("FAIL", "parsed", ipAddress);
        return ip;
    }

    /**
     * PASS: Use regex in the @PathVariable.
     *
     * URL:
     * http://localhost:8080/regex/10.32.14.15
     *
     * Expected:
     * {"method":"regex","ipAddress":"10.32.14.15"}
     */
    @RequestMapping(method = RequestMethod.GET, value = "/regex/{ipAddress:.+}")
    public @ResponseBody IpResponse regexIp(@PathVariable String ipAddress) {
        IpResponse ip = new IpResponse("PASS", "regex", ipAddress);
        return ip;
    }

    /**
     * PASS: Use the HttpServletRequest object to get the ip.
     *
     * URL:
     * http://localhost:8080/servlet/10.32.14.15
     *
     * Expected:
     * {"method":"servlet","ipAddress":"10.32.14.15"}
     */
    @RequestMapping(method = RequestMethod.GET, value = "/servlet/{ipAddress}")
    public @ResponseBody IpResponse servletIp(HttpServletRequest request) {
        String[] uriArray = request.getRequestURI().split("/");
        String uriPart=uriArray[uriArray.length-1];
        IpResponse ip = new IpResponse("PASS", "servlet", uriPart);
        return ip;
    }

    /**
     * PASS: Use a request parameter to retrieve the ip.
     *
     * URL:
     * http://localhost:8080/param/request?ipAddress=10.32.14.15
     *
     * Expected:
     * {"method":"request","ipAddress":"10.32.14.15"}
     */
    @RequestMapping(method = RequestMethod.GET, value = "/request")
    public @ResponseBody IpResponse requestIp(@RequestParam String ipAddress) {
        IpResponse ip = new IpResponse("PART", "request", ipAddress);
        return ip;
    }

}
