package com.ivystreet.spring.domain;

public class IpResponse {

    private String result;
    private String method;
    private String ipAddress;

    public IpResponse(String result, String method, String ipAddress) {
        this.result = result;
        this.method = method;
        this.ipAddress = ipAddress;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

}
