This example shows how Spring truncates an IP address passed in as a path variable, and three ways to correct this behavior.

Look in the file ParamController.java for the urls it tests and expected outcomes.

Prerequisites:
 -minimum JDK 1.6 (if using JDK 7 then change parameter java.version in the pom).
 -minimum Maven 3.0.5

 Jetty is included in the pom for running the application.

 To run execute:

    mvn clean install jetty:run

 Got to localhost:8080 and you will see the home page and links for testing.  The links will return some JSON.

 (There is also an IntelliJ iml file included, if you like to use that.)

Regards

 Bil Mowry
 Ivy Street Technology LLC
 August 2014


