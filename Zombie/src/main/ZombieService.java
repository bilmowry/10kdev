package main;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class ZombieService {
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void sortByLastName(List<Zombie> zombies) {
		Comparator comp = new ZombieComparator();
		Collections.sort(zombies, comp);
		printZombie(zombies);
	}

	private void printZombie(List<Zombie> zombies) {
		for (Zombie z : zombies) {
			System.out.println(z.getLastName());
		}
	}
}
