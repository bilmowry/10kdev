package main;

import java.util.Comparator;

@SuppressWarnings("rawtypes")
public class ZombieComparator implements Comparator {

	@Override
	public int compare(Object arg0, Object arg1) {
		Zombie o0 = (Zombie) arg0;
		Zombie o1 = (Zombie) arg1;
		return o0.getLastName().compareTo(o1.getLastName());
	}
}
