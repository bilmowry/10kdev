package test;

import java.util.ArrayList;
import java.util.List;

import main.Zombie;
import main.ZombieService;

import org.junit.Test;

public class ZombieServiceTest {

	@Test
	public void test() {
		List<Zombie> zombieList = new ArrayList<Zombie>();
		zombieList.add(new Zombie("Fred", "Munster"));
		zombieList.add(new Zombie("Count", "Dracula"));
		zombieList.add(new Zombie("Lilly", "Adams"));

		ZombieService zombieService = new ZombieService();
		zombieService.sortByLastName(zombieList);
	}

}
