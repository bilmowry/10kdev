package com.ivystreet.test;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This gives happy path test coverage for all the beans in packageName, denoted by a pojoQualifier.
 *
 * Errors on beans that fail are logged at debug level.
 *
 * As test coverage grows, add covered class names to excludes array.
 */
public class PackagePojoTest extends AbstractPojoTest
{

    public static final Logger LOG = LoggerFactory.getLogger(PackagePojoTest.class);

    /**
     * Run tests on all beans in PACKAGE via testPojo from AbstractPojoTest.
     *
     * @throws java.io.IOException
     * @throws java.lang.ClassNotFoundException
     */
    @SuppressWarnings("static-access")
	public void testPojos(String packageName, String pojoQualifier, String ... excludes) throws java.io.IOException, java.lang.ClassNotFoundException
    {
        Class[] classes = this.getClasses(packageName);

        for(Class clazz : classes)
        {
           if((clazz.getSimpleName().endsWith(pojoQualifier) || ("").equals(pojoQualifier))  && !Arrays.asList(excludes).contains(clazz.getSimpleName()))
           {
               try
               {
                   testPojo(clazz);
               }
               catch (AssertionError e)
               {
                   LOG.debug("Cannot test bean class", clazz);
               }
           }
        }

    }

    /**
     * Scans all classes accessible from the context class loader which belong to the given package and subpackages.
     *
     * @param packageName The base package
     * @return The classes
     * @throws ClassNotFoundException
     * @throws IOException
     */
    private static Class[] getClasses(String packageName)
            throws ClassNotFoundException, IOException
    {
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        assert classLoader != null;
        String path = packageName.replace('.', '/');
        Enumeration<URL> resources = classLoader.getResources(path);
        List<File> dirs = new ArrayList<File>();
        while (resources.hasMoreElements())
        {
            URL resource = resources.nextElement();
            dirs.add(new File(resource.getFile()));
        }
        ArrayList<Class> classes = new ArrayList<Class>();
        for (File directory : dirs)
        {
            classes.addAll(findClasses(directory, packageName));
        }
        return classes.toArray(new Class[classes.size()]);
    }

    /**
     * Recursive method used to find all classes in a given directory and subdirs.
     *
     * @param directory   The base directory
     * @param packageName The package name for classes found inside the base directory
     * @return The classes
     * @throws ClassNotFoundException
     */
    private static List<Class> findClasses(File directory, String packageName) throws ClassNotFoundException
    {
        List<Class> classes = new ArrayList<Class>();
        if (!directory.exists())
        {
            return classes;
        }
        File[] files = directory.listFiles();
        for (File file : files)
        {
            if (file.isDirectory())
            {
                assert !file.getName().contains(".");
                classes.addAll(findClasses(file, packageName + "." + file.getName()));
            }
            else if (file.getName().endsWith(".class"))
            {
                classes.add(Class.forName(packageName + '.' + file.getName().substring(0, file.getName().length() - 6)));
            }
        }
        return classes;
    }

}
