package com.ivystreet.test;

import java.beans.BeanInfo;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class AbstractPojoTest {
	
	public static final Logger LOG = LoggerFactory.getLogger(AbstractPojoTest.class);

	public final static String STRING = "foo";
	public final static int INTEGER_PRIM = 123;
	public final static Integer INTEGER_OBJ = 123;
	public final static double DOUBLE_PRIM = 123.0;
	public final static Double DOUBLE_OBJ = 123.0;
	public final static boolean BOOLEAN_PRIM = true;
	public final static Boolean BOOLEAN_OBJ = true;
	public final static java.util.Date DATE = new java.util.Date(1380209538000L);
	public final static BigDecimal USER_DECIMAL = new BigDecimal("1234.99");
	public final static long LONG_PRIM = 321L;
	public final static Long LONG_OBJ = 321L;

	private Map<Class<?>, Object> testValues = new HashMap<Class<?>, Object>();

	protected void addTestValue(Class<?> propertyType, Object testValue) {
		testValues.put(propertyType, testValue);
	}

	@Before
	public void setUpTestValues() throws Exception {

		addTestValue(String.class, STRING);
		addTestValue(int.class, INTEGER_PRIM);
		addTestValue(Integer.class, INTEGER_OBJ);
		addTestValue(double.class, DOUBLE_PRIM);
		addTestValue(Double.class, DOUBLE_OBJ);
		addTestValue(boolean.class, BOOLEAN_PRIM);
		addTestValue(Boolean.class, BOOLEAN_OBJ);
		addTestValue(java.util.Date.class, DATE);
		addTestValue(BigDecimal.class, USER_DECIMAL);
		addTestValue(long.class, LONG_PRIM);
		addTestValue(Long.class, LONG_OBJ);

	}

	@SuppressWarnings("unchecked")
	public <T> T generatePojo(Class<?> pojoClass, String... ignoreProperties) {
		try {
			Object pojo = pojoClass.newInstance();
			BeanInfo pojoInfo = Introspector.getBeanInfo(pojoClass);
			for (PropertyDescriptor propertyDescriptor : pojoInfo
					.getPropertyDescriptors()) {
				if (!Arrays.asList(ignoreProperties).contains(
						propertyDescriptor.getName())) {
					Class<?> propertyType = propertyDescriptor
							.getPropertyType();
					Object testValue = testValues.get(propertyType);
					if (testValue != null) {
						Method writeMethod = propertyDescriptor
								.getWriteMethod();
						Method readMethod = propertyDescriptor.getReadMethod();
						if (readMethod != null && writeMethod != null) {
							writeMethod.invoke(pojo, testValue);
							Assert.assertEquals(readMethod.invoke(pojo),
									testValue);
						}
					}

				}
			}
			return (T) pojo;
		} catch (Exception e) {
			String msg = "AbstractPojoTest.generatePojo() error: "
					+ e.getLocalizedMessage();
			LOG.debug(msg, pojoClass);
			Assert.fail(msg);
		}
		return null;
	}

	/*
	 * ignoreProperties not required Valid method calls: testPojo(pojoClass)
	 * testPojo(pojoClass, "propName1", "propName2")
	 */
	protected void testPojo(Class<?> pojoClass, String... ignoreProperties) {
		try {
			Object pojo = pojoClass.newInstance();
			BeanInfo pojoInfo = Introspector.getBeanInfo(pojoClass);
			for (PropertyDescriptor propertyDescriptor : pojoInfo
					.getPropertyDescriptors()) {
				if (!Arrays.asList(ignoreProperties).contains(
						propertyDescriptor.getName())) {
					testProperty(pojo, propertyDescriptor);
				}
			}
		} catch (Exception e) {
			String msg = "AbstractPojoTest.testPojo() error: "
					+ e.getLocalizedMessage();
			LOG.debug(msg, pojoClass);
			Assert.fail(msg);
		}
	}

	/*
	 * This method compares the properties between two objects that may not be
	 * linear, for example an entity and a dto.
	 * 
	 * Usage:
	 * 
	 * ignoreProperties not required Valid method calls: comparePojo(pojoClass)
	 * testCompare(pojo 1, pojo 2, "propName1", "propName2")
	 */
	protected void testCompare(Object pojo1, Object pojo2,
			String... ignoreProperties) {
		try {
			BeanInfo pojoInfo = Introspector.getBeanInfo(pojo1.getClass());
			for (PropertyDescriptor propertyDescriptor : pojoInfo
					.getPropertyDescriptors()) {
				if (!Arrays.asList(ignoreProperties).contains(
						propertyDescriptor.getName())) {
					compareProperty(pojo1, pojo2, propertyDescriptor);
				}
			}
		} catch (Exception e) {
			String msg = "AbstractPojoTest.testPojoProperties() error: "
					+ e.getLocalizedMessage();
			LOG.debug(msg, pojo1, pojo2);
			Assert.fail(msg);
		}
	}

	private void testProperty(Object pojo, PropertyDescriptor propertyDescriptor) {
		try {
			Class<?> propertyType = propertyDescriptor.getPropertyType();
			Object testValue = testValues.get(propertyType);
			if (testValue == null) {
				return;
			}
			Method writeMethod = propertyDescriptor.getWriteMethod();
			Method readMethod = propertyDescriptor.getReadMethod();
			if (readMethod != null && writeMethod != null) {
				writeMethod.invoke(pojo, testValue);
				Assert.assertEquals(readMethod.invoke(pojo), testValue);
			}
		} catch (Exception e) {
			String msg = "AbstractPojoTest.testProperty() error: "
					+ e.getLocalizedMessage();
			LOG.debug(msg, pojo);
			Assert.fail(msg);
		}
	}

	private void compareProperty(Object pojo1, Object pojo2,
			PropertyDescriptor propertyDescriptor) {
		try {
			Class<?> propertyType = propertyDescriptor.getPropertyType();
			Object testValue = testValues.get(propertyType);
			if (testValue == null) {
				return;
			}
			Method writeMethod = propertyDescriptor.getWriteMethod();
			Method readMethod = propertyDescriptor.getReadMethod();
			if (readMethod != null && writeMethod != null) {
				Assert.assertEquals(readMethod.invoke(pojo1),
						readMethod.invoke(pojo2));
			}
		} catch (Exception e) {
			String msg = "AbstractPojoTest.compareProperty() error: "
					+ e.getLocalizedMessage();
			LOG.debug(msg, pojo1, pojo2);
			Assert.fail(msg);
		}
	}
}