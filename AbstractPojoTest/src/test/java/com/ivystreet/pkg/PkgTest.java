package com.ivystreet.pkg;

import org.junit.Test;

import com.ivystreet.test.PackagePojoTest;

public class PkgTest extends PackagePojoTest {

	private String PACKAGE_NAME = "com.ivystreet.pkg";
	private String POJO_QUALIFIER = "";
	private String[] EXCLUDES = {"PackagePojoExcludes"};

	@Test
	public void testPackageBeans() throws java.io.IOException,
			java.lang.ClassNotFoundException {
		testPojos(PACKAGE_NAME, POJO_QUALIFIER, EXCLUDES);
	}
}
