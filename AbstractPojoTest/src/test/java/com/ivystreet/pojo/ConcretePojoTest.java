package com.ivystreet.pojo;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.ivystreet.test.AbstractPojoTest;

public class ConcretePojoTest extends AbstractPojoTest {

	/**
	 * This tests all the properties, but we exclude the property with the
	 * custom getter.
	 */
	@Test
	public void testProperties() {
		testPojo(ConcretePojo.class, "breaks");
	}

	/**
	 * This shows what happens if you don't exclude the custom getter.
	 */
	@Test(expected = AssertionError.class)
	public void testPropertiesBreaks() {
		testPojo(ConcretePojo.class);
	}

	/**
	 * In this example we test the custom getter.
	 */
	@SuppressWarnings("static-access")
	@Test(expected = AssertionError.class)
	public void testCustomGetter() {
		ConcretePojo pojo = generatePojo(ConcretePojo.class);
		Integer expected = this.INTEGER_OBJ + ConcretePojo.OOPS_LOGIC;
		assertEquals(expected, pojo.getBreaks());
	}

	/**
	 * Your choice here is to either add the custom object to AbstractPojoTest,
	 * or write a separate test.
	 * 
	 * Also we test the custom constructor that takes the key object.
	 * 
	 * We use generatePojo to generate bootstrap data for this test as well.
	 */
	@SuppressWarnings("static-access")
	@Test
	public void testKeyAndConstructor() {
		ConcretePojoKey key = generatePojo(ConcretePojoKey.class);
		ConcretePojo pojo = new ConcretePojo(key);
		assertEquals(this.LONG_OBJ, pojo.getKey().getKeyId());
	}
	
}
