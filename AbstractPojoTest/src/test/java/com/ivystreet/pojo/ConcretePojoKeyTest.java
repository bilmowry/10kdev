package com.ivystreet.pojo;

import org.junit.Test;

import com.ivystreet.test.AbstractPojoTest;

public class ConcretePojoKeyTest extends AbstractPojoTest {

	/**
	 * The simplest of test classes.
	 */
	@Test
	public void testProperties() {
		testPojo(ConcretePojoKey.class);
	}

}
