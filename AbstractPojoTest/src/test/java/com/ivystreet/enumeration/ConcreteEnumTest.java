package com.ivystreet.enumeration;

import org.junit.Test;

import com.ivystreet.test.AbstractEnumTest;

public class ConcreteEnumTest extends AbstractEnumTest {
	
	/**
	 * Testing enum constants and values helps make sure people don't add things
	 * without breaking something; it makes their code deliberate.
	 */
	@Test
	public void testConstants() {
		testEnumConstants(ConcreteEnum.class, "B", "O", "IP", "C");
	}
	
	@Test
	public void testValues() {
		testEnumConstants(ConcreteEnum.class, "B", "O", "IP", "C");
	}

}
