package com.ivystreet.pkg.error;

public class PackagePojoError {
	
	private Long id;
	private String value;
	
	public static final Long OOPS = 1L;
	
	public Long getId() {
		return id + OOPS;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	
}
