package com.ivystreet.pkg.excludes;

public class PackagePojoExcludes {
	
	private Long id;
	private String value;
	
	public PackagePojoExcludes() {
		System.out.println("PackagePojoExcludes has been created.");
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	
}
