package com.ivystreet.pojo;

public class ConcretePojoKey {
	
	private Long keyId;
	
	public ConcretePojoKey() {
		//intentional
	}
	
	public ConcretePojoKey(Long keyId) {
		this.keyId = keyId;
	}

	public Long getKeyId() {
		return keyId;
	}

	public void setKeyId(Long keyId) {
		this.keyId = keyId;
	}
	
}
