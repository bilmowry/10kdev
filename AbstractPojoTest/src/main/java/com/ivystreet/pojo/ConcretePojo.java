package com.ivystreet.pojo;

public class ConcretePojo {

	private ConcretePojoKey key;
	private Long index;
	private String value;
	private Integer breaks;

	public static final Integer OOPS_LOGIC = 66;
	
	
	public ConcretePojo() {
		//intentional
	}
	
	public ConcretePojo(ConcretePojoKey key) {
		this.key = key;
	}

	public ConcretePojoKey getKey() {
		return key;
	}

	public void setKey(ConcretePojoKey key) {
		this.key = key;
	}

	public Long getIndex() {
		return index;
	}

	public void setIndex(Long index) {
		this.index = index;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	/**
	 * 
	 * @return this does some other logic and will break the standard pojo test
	 */
	public Integer getBreaks() {
		return breaks + OOPS_LOGIC;
	}

	public void setBreaks(Integer breaks) {
		this.breaks = breaks;
	}
}
