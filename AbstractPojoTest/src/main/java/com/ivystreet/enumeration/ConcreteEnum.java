package com.ivystreet.enumeration;

public enum ConcreteEnum {

	B("Backlog"), O("Open"), IP("In Progress"), C("Complete");

	private String value;

	private ConcreteEnum(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

}
