AbstractPojoTest project shows an example of using two testers to get code coverage an fault-tolerance on pojo classes in a quick-to-test manner.

Prerequisites:
 -minimum JDK 1.7 (you can probably get by with jdk 6).
 -minimum Maven 3.0.5

To run execute:

    mvn clean install

Also Eclipse Luna project files are included.

TODO:
Add code:
-for hashCode Equals
-nullsafetty : assertFalse(pojo.equals(null))
-reflexivity : assertTrue(pojo.equals(pojo))
-classcastsafe : assertFalse(pojo.equals(�dsfsdf�))
-toString against nullsafety

Idea originally from http://danhaywood.com/2010/06/11/pojo-properties-auto-testing/.

Regards

 Bil Mowry
 Ivy Street Technology LLC
 October 2014
