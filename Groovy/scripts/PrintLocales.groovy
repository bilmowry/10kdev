/*
	Print all locales
*/
import java.text.SimpleDateFormat;

def localeList = SimpleDateFormat.getAvailableLocales();

localeList.each{
    println "{$it}"
}
