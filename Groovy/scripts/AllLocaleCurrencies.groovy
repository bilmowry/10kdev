/*
	Print out the currency formats for all possible locales.
 */

import java.text.NumberFormat
import java.text.SimpleDateFormat

def moneyFormat = { locale, amount ->
    Double currencyAmount = new Double(amount)
    Currency currentCurrency = Currency.getInstance(locale)
    NumberFormat currencyFormatter = NumberFormat.getCurrencyInstance(locale)
    print(locale.getDisplayName() + ", " +
            currentCurrency.getDisplayName() + ": " +
            currencyFormatter.format(currencyAmount))
}

//print all locale currencies
def printAllCurrencies = {
    def localeList = SimpleDateFormat.getAvailableLocales()
    localeList.each {
        def language = it?.language
        def country = it?.country
        def amount = 3567.14
        println ""
        print language + " " + country + " "
        if (language?.trim() && country?.trim()) {
            moneyFormat(it, amount)
        } else {
            print it.displayName
        }
    }

}

printAllCurrencies()