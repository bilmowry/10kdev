Integer testNumber = -456987

public String firstDigitIterative(Integer n) {
  while (n < -9 || 9 < n) n /= 10;
  return Math.abs(n);
}

public String firstDigitRecursive(Integer n) {
  if (n >= -9 && 9 >= n) return Math.abs(n);
  return firstDigitRecursive((n / 10).toInteger());
}

public Integer firstDigitShift(Integer n) {
  n = Math.abs(n)
  int x = (number >> (8*0)) & 0xff
  return x
}

public String firstDigitLog(Integer n) {
  while (n < -9 || 9 < n) n /= 10;
  return Math.abs(n);
}

public String firstDigitString(Integer n) {
  return Math.abs(n).toString().toCharArray()[0];
}


//
//println "Iterative Method - " + firstDigitIterative(testNumber)
println "Recursive Method - " + firstDigitRecursive(testNumber)
// println "Shift Method - " + firstDigitShift(testNumber)
//println "String Method - " + firstDigitString(testNumber)



//strange

public int firstDigitStrange(Integer n) {
  return Math.abs(n).toString().toCharArray()[0];
}

//println "Strange Method - " + firstDigitStrange(testNumber)