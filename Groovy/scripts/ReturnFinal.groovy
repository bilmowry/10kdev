class E1 extends Exception{}
class E2 extends Exception{}

def testThis() {
  println "start of method"
    try {
       //throw new E1()
        println "try"
        //assert false
      throw new E2()
      return 0
    } catch (E1 e) {
       println "catch 1"
       return 1
    } catch (E2 e) {
       println "catch 2"
       return 2
    } finally {
       println "finally"
       return 3
    }
  println "end of method"
  return 4
}


testThis()