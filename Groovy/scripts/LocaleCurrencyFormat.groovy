/*
	Print out the currency formats for locales.
 */

import java.text.NumberFormat

def moneyFormat = { language, country, amount ->
    Locale currentLocale = new Locale(language, country);
    Double currencyAmount = new Double(amount);
    Currency currentCurrency = Currency.getInstance(currentLocale);
    NumberFormat currencyFormatter = NumberFormat.getCurrencyInstance(currentLocale);
    println(currentLocale.getDisplayName() + ", " +
            currentCurrency.getDisplayName() + ": " +
            currencyFormatter.format(currencyAmount));
}

def printSomeCurrencies {
	println ""
	moneyFormat("fr", "BE", 2567.14)
	moneyFormat("nl", "BE", 2567.14)
	moneyFormat("nl", "NL", 2567.14)
	moneyFormat("fr", "FR", 2567.14)
	moneyFormat("en", "GB", 2567.14)
	moneyFormat("en", "US", 2567.14)
}

printSomeCurrencies
